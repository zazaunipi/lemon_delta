Delta Model
===========

This package is part of the Lemon Delta project suite. It contains mainly the URDF and geometries of the Delta robot.