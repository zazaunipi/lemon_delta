#include <delta_kinematics/delta_ik.h>

using namespace delta_kinematics;

int main(int argc, char **argv)
{
    // Measures were taken by visual inspection ;)
    // ToDo: URDF parsing to build the robot :O
    double cos60 =  0.5;
    double sin60 = 0.866025404;
    DeltaLeg lemon_leg;
    lemon_leg.a_x = -sin60;
    lemon_leg.a_y = cos60;
    lemon_leg.a_z = 0.0;
    lemon_leg.u0_x = 0.0;
    lemon_leg.u0_y = 0.0;
    lemon_leg.u0_z = 1.0;
    lemon_leg.b_x = 0.13*cos60;
    lemon_leg.b_y = 0.13*sin60;
    lemon_leg.b_z = 0.0;
    lemon_leg.p_x = 0.04*cos60;
    lemon_leg.p_y = 0.04*sin60;
    lemon_leg.p_z = 0.0;
    lemon_leg.l_b = 0.13;
    lemon_leg.l_p = 0.16;
    lemon_leg.angle_min = -M_PI/8.0;
    lemon_leg.angle_max = M_PI/6.0;

    DeltaRobot lemon;
    createDeltaRobot(lemon_leg, lemon);

    DeltaIKSolver lemon_ik(lemon);

    double a,b,c; //angles
    lemon_ik.CartToJnt(0.0, 0.0, 0.01,a,b,c);

	return 0;
}
