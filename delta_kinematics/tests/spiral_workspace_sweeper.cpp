#include <string>
#include <ros/console.h>
#include <ros/ros.h>
#include <geometry_msgs/Point.h>
#include <cmath>
#include <delta_kinematics/SpiralStart.h>
#include <thread>
#include <chrono>

double radius(0.05);
double zmin(0.22), zmax(0.25);
bool perform_spiral(false);
double theta(0.0);
double vert_pass(0.02); //2cm per round
double tot_time(5); //5s for the whole spiral
double theta_final(((zmax-zmin)/vert_pass)*2*M_PI);
double theta_pass(theta_final/(tot_time*50)); //50 is frequency rate

ros::Publisher pub;
inline double getHeight(double angle)
{
    return (vert_pass*angle/(2*M_PI) + zmin);
}

bool
cbStartSpiral(delta_kinematics::SpiralStart::Request &req, delta_kinematics::SpiralStart::Response &res )
{
    if(req.radius > 0 && req.zmin >0 && req.zmax>0
            && req.vert_p >0 && req.tot_t >0){
        radius = req.radius;
        zmin = req.zmin;
        zmax = req.zmax;
        vert_pass = req.vert_p;
        tot_time = req.tot_t;
    }
    else
        ROS_WARN("Invalid Request, using last valid one!");
    theta_final = ((zmax - zmin)/vert_pass)*2*M_PI;
    theta_pass = theta_final/(tot_time*50);
    //reinit theta
    theta = 0.0;
    perform_spiral = true;
    //send it to initial position and wait 3 seconds
    geometry_msgs::Point initial;
    initial.x = radius;
    initial.y = 0;
    initial.z = zmin;
    pub.publish(initial);
    std::this_thread::sleep_for(std::chrono::seconds(3));
    ros::spinOnce();

    return true;
}

void spiralStep()
{
    if (perform_spiral){
        if (theta > theta_final){
            // we did the spiral! Wait for a new service call
            theta = 0;
            perform_spiral = false;
            ROS_INFO("Spiral complete !");
            return;
        }
        geometry_msgs::Point desired;
        desired.x = radius*cos(theta);
        desired.y = radius*sin(theta);
        desired.z = getHeight(theta);
        theta += theta_pass;
        pub.publish(desired);
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "spiral_workspace_sweeper");
    ros::NodeHandle nh;
    pub = nh.advertise<geometry_msgs::Point>("/lemon_delta_kinematics/desired_cartesian", 5);
    ros::ServiceServer srv = nh.advertiseService("start_spiral", &cbStartSpiral);
    ros::Rate rate(50);
    while (ros::ok())
    {
        spiralStep();
        ros::spinOnce();
        rate.sleep();
    }
    return 0;
}
